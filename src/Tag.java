/**
 * @author aleksruci on 28/Feb/2019
 */
public class Tag implements Comparable<Tag>{
    private String description;

    public Tag(String description) {
        this.description = description;
    }

    @Override
    public int compareTo(Tag o) {
        return this.description.compareTo(o.getDescription());
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
