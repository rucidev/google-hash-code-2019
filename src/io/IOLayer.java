package io;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * @author aleksruci on 28/Feb/2019
 */
public class IOLayer {
    // path + name + extension of the file
    private String inputPath;
    private String outputPath;

    public IOLayer(int type) throws IOException{
        this.setType(type);
    }

    /**
     * Determines the strategy to be used in order to read the file
     * depending on the size of that file.
     *
     * @return a list of all the lines in the file
     */
    public List<String> readFile() throws IOException {
        if(inputPath == null)
            return null;

        switch (inputPath) {
            case FileType.Constants.EXAMPLE_INPUT_PATH:
                return readSmallFile();
            case FileType.Constants.SMALL_INPUT_PATH:
                return readSmallFile();
            case FileType.Constants.MEDIUM_INPUT_PATH:
                return readLargeFile();
            case FileType.Constants.BIG_INPUT_PATH:
                return readLargeFile();
            default:
                return null;
        }
    }

    /**
     * This method uses BufferedReader since it is fast and has a buffer of
     * 8 KB which makes it an appropriate choice for our purpose of reading
     * the big input type.
     *
     * @return a list of all the lines in the file
     */
    private List<String> readLargeFile() throws IOException {
        BufferedReader reader = Files.newBufferedReader(Paths.get(this.inputPath), StandardCharsets.UTF_8);

        List<String> lines = new ArrayList<>();
        String line;
        while ((line = reader.readLine()) != null) {
            lines.add(line);
        }
        return lines;
    }

    /**
     * For small (as specified in the javadoc) and fairly large files
     * this method(Files.readAllLines) is convenient in order to
     * read small input type.
     *
     * @return a list of all the lines in the file
     */
    private List<String> readSmallFile() throws IOException {
        return Files.readAllLines(Paths.get(this.inputPath), StandardCharsets.UTF_8);
    }

    /**
     * Determines the strategy to be used in order to write the file
     * depending on the size of the data.
     *
     */
    public void writeFile(List<String> lines) throws IOException {
        if(inputPath == null)
            return;

        switch (inputPath) {
            case FileType.Constants.EXAMPLE_INPUT_PATH:
                writeSmallFile(lines);
                break;
            case FileType.Constants.SMALL_INPUT_PATH:
                writeSmallFile(lines);
                break;
            case FileType.Constants.MEDIUM_INPUT_PATH:
                writeLargeFile(lines);
                break;
            case FileType.Constants.BIG_INPUT_PATH:
                writeLargeFile(lines);
                break;
            default:
                break;
        }
    }

    /**
     * This method uses BufferedWriter since it is fast and has a buffer of
     * 8 KB which makes it an appropriate choice for our purpose of writing
     * big amount of data.
     *
     */
    private void writeLargeFile(List<String> lines) throws IOException {
        try (BufferedWriter writer = Files.newBufferedWriter(Paths.get(this.outputPath), StandardCharsets.UTF_8)){
            for(String line : lines){
                writer.write(line);
                writer.newLine();
            }
        }
    }

    /**
     * For small (as specified in the javadoc) and fairly large data
     * this method(Files.write) is convenient in order to
     * write small amount of data.
     *
     */
    private void writeSmallFile(List<String> lines) throws IOException {
        Files.write(Paths.get(this.outputPath), lines, StandardCharsets.UTF_8);
    }

    public static void log(Object object) {
        System.out.println(object);
    }

    public void setType(int type) throws IOException{
        this.inputPath = FileType.getInputPathById(type);
        if(inputPath == null)
            throw new IOException("No file found with specified type");

        this.outputPath = FileType.getOutputPathById(type);
    }
}
