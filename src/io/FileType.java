package io;

/**
 * @author aleksruci on 28/Feb/2019
 */
public enum FileType {
    EXAMPLE(Constants.EXAMPLE_ID, Constants.EXAMPLE_INPUT_PATH, Constants.EXAMPLE_OUTPUT_PATH),
    SMALL(Constants.SMALL_ID, Constants.SMALL_INPUT_PATH, Constants.EXAMPLE_OUTPUT_PATH),
    MEDIUM(Constants.MEDIUM_ID, Constants.MEDIUM_INPUT_PATH, Constants.EXAMPLE_OUTPUT_PATH),
    BIG(Constants.BIG_ID, Constants.BIG_INPUT_PATH, Constants.EXAMPLE_OUTPUT_PATH),
    SHINY(Constants.SHINY_ID, Constants.SHINY_INPUT_PATH, Constants.SHINY_OUTPUT_PATH);

    private final int id;
    private final String inputPath;
    private final String outputPath;

    FileType(int id, String inputPath, String outputPath) {
        this.id = id;
        this.inputPath = inputPath;
        this.outputPath = outputPath;
    }

    public static String getInputPathById(int id) {
        switch (id) {
            case Constants.EXAMPLE_ID:
                return Constants.EXAMPLE_INPUT_PATH;
            case Constants.SMALL_ID:
                return Constants.SMALL_INPUT_PATH;
            case Constants.MEDIUM_ID:
                return Constants.MEDIUM_INPUT_PATH;
            case Constants.BIG_ID:
                return Constants.BIG_INPUT_PATH;
            default:
                return null;
        }
    }

    public static String getOutputPathById(int id) {
        switch (id) {
            case Constants.EXAMPLE_ID:
                return Constants.EXAMPLE_OUTPUT_PATH;
            case Constants.SMALL_ID:
                return Constants.SMALL_OUTPUT_PATH;
            case Constants.MEDIUM_ID:
                return Constants.MEDIUM_OUTPUT_PATH;
            case Constants.BIG_ID:
                return Constants.BIG_OUTPUT_PATH;
            default:
                return null;
        }
    }

    public int getId() {
        return this.id;
    }

    public final String getInputPath() {
        return this.inputPath;
    }

    public final String getOutputPath() {
        return this.outputPath;
    }

    static class Constants {
        static final int EXAMPLE_ID = 1;
        static final int SMALL_ID = 2;
        static final int MEDIUM_ID = 3;
        static final int BIG_ID = 4;
        static final int SHINY_ID = 5;

        static final String EXAMPLE_INPUT_PATH = "src/input/a_example.txt";
        static final String SMALL_INPUT_PATH = "src/input/b_lovely_landscapes.txt";
        static final String MEDIUM_INPUT_PATH = "src/input/c_memorable_moments.txt";
        static final String BIG_INPUT_PATH = "src/input/d_pet_pictures.txt";
        static final String SHINY_INPUT_PATH = "src/input/e_shiny_selfies.txt";

        static final String EXAMPLE_OUTPUT_PATH = "src/output/a_example.txt";
        static final String SMALL_OUTPUT_PATH = "src/output/b_lovely_landscapes.txt";
        static final String MEDIUM_OUTPUT_PATH = "src/output/c_memorable_moments.txt";
        static final String BIG_OUTPUT_PATH = "src/output/d_pet_pictures.txt";
        static final String SHINY_OUTPUT_PATH = "src/output/e_shiny_selfies.txt";
    }
}
