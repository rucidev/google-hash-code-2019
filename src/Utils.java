import java.util.List;

/**
 * @author aleksruci on 28/Feb/2019
 */
public class Utils {
    public static<T> void printArray(T[] array) {
        for (T element : array) {
            System.out.print(element + " ");
        }
        System.out.println();
    }

    public static<T> void printMatrix(T[][] matrix) {
        for(int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; i++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

    private static char[][] getCharMatrixFromStrings(int rows, int cols, List<String> strings) {
        final char[][] matrix = new char[rows][cols];

        for (int i = 0; i < matrix.length; i++) {
            char[] line = strings.get(i).toCharArray();
            System.arraycopy(line, 0, matrix[i], 0, line.length);
        }
        return matrix;
    }
}
