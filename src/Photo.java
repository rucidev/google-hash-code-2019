import java.util.*;

/**
 * @author aleksruci on 28/Feb/2019
 */
public class Photo {
    private int id;
    private Set<Tag> tags;
    private Orientation orientation;

    public Photo(int id, Orientation orientation, int nrTags) {
        this.id = id;
        this.orientation = orientation;
        this.tags = new LinkedHashSet<>(nrTags);
    }

    public boolean addTag(Tag tag) {
        return tags.add(tag);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Orientation getOrientation() {
        return orientation;
    }

    public void setOrientation(Orientation orientation) {
        this.orientation = orientation;
    }

    public Set<Tag> getTags() {
        return tags;
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    public enum Orientation {
        HORIZONTAL, VERTICAL;
    }
}
