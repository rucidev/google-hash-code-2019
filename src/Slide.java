import java.util.ArrayList;
import java.util.Set;

/**
 * @author aleksruci on 28/Feb/2019
 */
public class Slide {
    private Photo photo1;
    private Photo photo2;

    public Slide(Photo photo) {
        this.photo1 = photo;
    }

    public Slide(Photo photo, Photo photo2) {
        this.photo1 = photo1;
        this.photo2 = photo2;
    }

    public Set<Tag> getTags() {
        Set<Tag> tags1 = photo1.getTags();

        Set<Tag> tags2;
        if(photo2 != null) {
            tags2 = photo2.getTags();
            tags1.addAll(tags2);
        }

        return tags1;
    }
}
